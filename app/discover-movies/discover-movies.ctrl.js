"use strict";

function discoverMoviesCtrl(ApiService, $timeout) {
  const ctrl = this;
  ctrl.currentPage = 1;
  ctrl.moviesToDiscover = [];
  ctrl.busy = false;

  ctrl.loadMore = () => {
    if (ctrl.busy) return;
    ctrl.busy = true;

    ApiService.getMoviesToDiscover(ctrl.currentPage).then(response => {
      ++ctrl.currentPage;
      ctrl.moviesToDiscover = ctrl.moviesToDiscover.concat(response.results);
      $timeout(() => {
        ctrl.busy = false;
      }, 1000);
    });
  };

  ctrl.loadMore();
}

angular
  .module("myApp.discoverMovies", ["ngRoute"])
  .controller("DiscoverMoviesCtrl", [
    "ApiService",
    "$timeout",
    discoverMoviesCtrl
  ]);
