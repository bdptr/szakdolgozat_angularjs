"use strict";

function topRatedMoviesCtrl(ApiService) {
  const ctrl = this;
  ctrl.topRatedMovies = [];

  ApiService.getTopRatedMovies().then(response => {
    ctrl.topRatedMovies = response.results;
    console.log(ctrl.topRatedMovies);
  });
}

angular
  .module("myApp.topRatedMovies", ["ngRoute"])
  .controller("TopRatedMoviesCtrl", ["ApiService", topRatedMoviesCtrl]);
