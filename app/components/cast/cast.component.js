"use strict";

const castTemplateImdb = `
    <h4>Cast</h4>
    
    <table class="w-100">
        <tbody>
            <tr ng-repeat="item in cCtrl.castList track by $index">
                <td class="w-50">
                    <div class="d-flex align-items-center">
                        <img class="mr-2" src="http://image.tmdb.org/t/p/w45{{item.profile_path}}" ng-if="item.profile_path != null" />
                        <img class="mr-2" src="/resources/no-poster-w45.png" ng-if="item.profile_path == null" />

                        <a ng-bind="item.name"></a>
                    </div>
                </td>

                <td class="w-50" ng-bind="item.character"></td>
            </tr>
        </tbody>
    </table>
`;

const castTemplateMd = `
<ul class="collection collection--md">
    <li class="collection-item collection-item--md avatar" ng-repeat="item in cCtrl.castList track by $index">
        <div class="circle">
            <img src="http://image.tmdb.org/t/p/w45{{item.profile_path}}" alt="{{item.name}}" ng-if="item.profile_path">
            <img src="/resources/no-poster-w45-grey.png" alt="{{item.name}}" ng-if="!item.profile_path">
        </div>

        <span class="title" ng-bind="item.name"></span>
        <div class="grey-text" ng-bind="item.character"></div>
    </li>
</ul>
`;

const castTemplateMasonry = `
    <div masonry masonry-options="cCtrl.masonryOptions">
        <div class="grid-sizer"></div>
        <div class="gutter-sizer"></div>
        <div class="masonry-brick" ng-repeat="item in cCtrl.castList track by $index">
            <div class="card card--masonry">
                <img class="card-img-top" src="http://image.tmdb.org/t/p/w185{{item.profile_path}}" alt="{{item.name}}" ng-show="item.profile_path">
                <img class="card-img-top" src="/resources/no-poster-w185.png" alt="{{item.name}}" ng-show="!item.profile_path">

                <div class="card-body">
                    <div class="card-title m-0" ng-bind="item.name"></div>
                    <small ng-bind="item.character"></small>
                </div>
            </div>
        </div>
    </div>
`;

const castTemplateFlat = `
    <div class="row m-0">
        <div class="col-6 col-md-3 d-flex justify-content-center p-0 mb-4 position-relative" ng-repeat="item in cCtrl.castList track by $index">
            <div class="flip-card flip-card--w154 flat-scale"> 
                <div class="flip-card__inner flat-box-shadow" ng-click="cCtrl.flip($event);"> 
                    <div class="flip-card__face flip-card__face--front">
                        <img src="http://image.tmdb.org/t/p/w154{{item.profile_path}}" ng-if="item.profile_path" />
                        <img src="/resources/no-poster-w154.png" ng-if="!item.profile_path" />

                        <div class="flip-card__title flat-bg-{{cCtrl.colorTheme}}" ng-bind="item.name"></div>
                    </div>

                    <div class="flip-card__face flip-card__face--back p-0 flat-bg-{{cCtrl.colorTheme}} text-center">
                        <div class="p-2 font-size-1-3" ng-bind="item.character"></div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
`;

const castCtrl = function() {
  const cCtrl = this;

  cCtrl.$onInit = () => {
    cCtrl.masonryOptions = masonryOptions;
  };

  cCtrl.flip = e => {
    $(e.currentTarget).toggleClass("flipped");
  };
};

angular.module("myApp").component("cast", {
  controller: castCtrl,
  controllerAs: "cCtrl",
  template: getComponentTemplate({
    imdb: castTemplateImdb,
    md: castTemplateMd,
    masonry: castTemplateMasonry,
    flat: castTemplateFlat
  }),
  bindings: {
    castList: "=",
    colorTheme: "@?"
  }
});
