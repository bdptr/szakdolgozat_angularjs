"use strict";

const pageHeaderTemplateImdb = `
    <div class="page-header--imdb d-flex align-items-center px-4 py-2" style="height: 90px;">
        <a class="page-header__logo--imdb font-size-2 font-weight-bold mr-4 text-decor-none" href="#!/home">MDb</a>

        <div class="col d-flex flex-column h-100 p-0">
            <div class="col d-flex mb-2 p-0">
                <input class="d-block w-100 px-2" type="text" ng-model="phCtrl.searchValue" ng-enter="phCtrl.search()" />

                <div class="d-inline-flex align-items-center pointered px-3" ng-click="phCtrl.search()">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </div>
            </div>

            <div class="col d-flex align-items-center p-0">
                <div class="mr-4 dropdown" ng-repeat="menu in phCtrl.menus track by $index">
                    <a class="dropdown-toggle pointered" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" ng-bind="::menu.label"></a>
                    <div class="dropdown-menu page-header-dropdown-menu--imdb">
                        <a class="dropdown-item page-header-dropdown-item--imdb" ng-repeat="child in menu.children" href="{{child.location}}" ng-bind="::child.label"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
`;

const pageHeaderTemplateMd = `
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper nav-wrapper--md blue darken-3">
        <a class="navbar-menu-toggle--md" href="#" data-activates="side-nav-md" data-sidenav="left" data-menuwidth="300" data-closeonclick="true">
          <i class="material-icons">menu</i>
        </a>

        <form class="navbar-form--md">
          <div class="input-field">
            <input id="search" type="search" ng-model="phCtrl.searchValue" ng-enter="phCtrl.search()">

            <label class="label-icon" for="search">
              <i class="material-icons">search</i>
            </label>
            
            <i class="material-icons">close</i>
          </div>
        </form>
      </div>
    </nav>
  </div>
`;

const pageHeaderTemplateMasonry = `
    <nav class="page-header--masonry navbar fixed-top d-flex">
        <a id="logo" class="d-flex align-items-center mr-4" href="#!/home">MovieDataBase</a>

        <div class="d-flex align-items-center">
          <div class="mr-4 dropdown dropdown--masonry" ng-repeat="menu in phCtrl.menus track by $index">
              <i class="dropdown-toggle dropdown-toggle--masonry fa {{menu.icon}}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" />

              <div class="dropdown-menu dropdown-menu--masonry">
                  <a class="dropdown-item dropdown-item--masonry" ng-repeat="child in menu.children" href="{{child.location}}" ng-bind="::child.label"></a>
              </div>
          </div>
        </div>

        <input class="page-header--masonry__search col p-3 border-0 font-weight-bold" type="text" ng-model="phCtrl.searchValue" ng-enter="phCtrl.search()" />
    </nav>
`;

const pageHeaderTemplateFlat = `
  <nav class="navbar fixed-top page-header--flat flat-box-shadow">
    <div class="dropdown dropdown--flat">
      <i class="dropdown-toggle dropdown-toggle--flat fa fa-bars" aria-hidden="true" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></i>

      <div class="dropdown-menu dropdown-menu--flat flat-box-shadow">
        <div class="dropdown-menu--flat__column p-0">
          <div class="mb-2">Home</div>

          <a class="dropdown-item dropdown-item--flat flat-bg-yellow" href="#!/home">
            <i class="fa fa-home" aria-hidden="true"></i>
            <div>Home</div>
          </a>
        </div>

        <div class="dropdown-menu--flat__column p-0">
          <div class="mb-2">Movies</div>

          <a class="dropdown-item dropdown-item--flat flat-bg-red" href="#!/top-rated-movies">
            <i class="fa fa-heart" aria-hidden="true"></i>
            <div>Top Rated</div>
          </a>

          <a class="dropdown-item dropdown-item--flat flat-bg-green" href="#!/discover-movies">
            <i class="fa fa-compass" aria-hidden="true"></i>
            <div>Discover</div>
          </a>
        </div>

        <div class="dropdown-menu--flat__column p-0">
          <div class="mb-2">TV Shows</div>

          <a class="dropdown-item dropdown-item--flat flat-bg-blue" href="#!/top-rated-shows">
            <i class="fa fa-heart" aria-hidden="true"></i>
            <div>Top Rated</div>
          </a>

          <a class="dropdown-item dropdown-item--flat flat-bg-orange" href="#!/discover-shows">
            <i class="fa fa-compass" aria-hidden="true"></i>
            <div>Discover</div>
          </a>
        </div>
      </div>
    </div>

    <div class="page-header-search--flat">
      <label for="search">
        <i class="fa fa-search" aria-hidden="true"></i>
      </label>

      <input id="search" type="text" ng-model="phCtrl.searchValue" ng-enter="phCtrl.search()" />
    </div>
  </nav>
`;

const pageHeaderCtrl = function(SearchService, $location, $rootScope) {
  const phCtrl = this;

  phCtrl.$onInit = () => {
    phCtrl.menus = [
      {
        id: 1,
        label: "Movies",
        icon: "fa-video-camera",
        children: [
          {
            id: 1,
            label: "Top Rated Movies",
            location: "#!/top-rated-movies"
          },
          {
            id: 2,
            label: "Discover Movies",
            location: "#!/discover-movies"
          }
        ]
      },
      {
        id: 2,
        label: "TV Shows",
        icon: "fa-television",
        children: [
          {
            id: 1,
            label: "Top Rated TV Shows",
            location: "#!/top-rated-shows"
          },
          {
            id: 2,
            label: "Discover TV Shows",
            location: "#!/discover-shows"
          }
        ]
      }
    ];

    $rootScope.$on("SearchService.setSearchValue", () => {
      phCtrl.searchValue = SearchService.getSearchValue();
    });
  };

  phCtrl.search = () => {
    let joinedSearchValue = phCtrl.searchValue.split(" ").join("+");
    $location.path(`/search/${joinedSearchValue}`);
  };
};

angular.module("myApp").component("pageHeader", {
  controller: pageHeaderCtrl,
  controllerAs: "phCtrl",
  template: getComponentTemplate({
    imdb: pageHeaderTemplateImdb,
    md: pageHeaderTemplateMd,
    masonry: pageHeaderTemplateMasonry,
    flat: pageHeaderTemplateFlat
  })
});
