"use strict";

const ratingTemplateImdb = `
`;

const ratingTemplateMd = `
  <div class="d-flex justify-content-end align-items-center grey-text ml-2" ng-if="rCtrl.rating > 0">
    <div ng-bind="rCtrl.rating"></div>

    <i class="material-icons ml-1"
      ng-class="{
            'red-text text-darken-1': rCtrl.rating >= 7
        }">favorite</i>
  </div>
`;

const ratingTemplateMasonry = `
    <div class="vote_avg--masonry {{rCtrl.size}}"
         ng-class="{
             'vote_avg--masonry--nice': rCtrl.rating >= 7,
             'vote_avg--masonry--bleh': rCtrl.rating < 5
         }"
         ng-bind="rCtrl.rating"
         ng-show="rCtrl.rating > 0">
    </div>
`;

const ratingTemplateFlat = `
  <div class="vote_avg--flat flat-bg-{{rCtrl.colorTheme}}" ng-bind="rCtrl.rating" ng-show="rCtrl.rating && rCtrl.rating > 0"></div>
`;

const ratingCtrl = function() {
  const rCtrl = this;

  rCtrl.$onInit = () => {
    rCtrl.size = rCtrl.size || "";
  };
};

angular.module("myApp").component("rating", {
  controller: ratingCtrl,
  controllerAs: "rCtrl",
  template: getComponentTemplate({
    imdb: ratingTemplateImdb,
    md: ratingTemplateMd,
    masonry: ratingTemplateMasonry,
    flat: ratingTemplateFlat
  }),
  bindings: {
    rating: "<",
    max: "<?",
    size: "@?",
    colorTheme: "@?"
  }
});
