"use strict";

const similarShowsTemplateImdb = `
    <h4>Similar Shows</h4>

    <div class="d-flex align-items-center mb-2" ng-repeat="item in ssCtrl.similarShows">
        <img class="mr-2" src="http://image.tmdb.org/t/p/w92{{item.backdrop_path}}" ng-if="item.backdrop_path != null" />
        <img class="mr-2" src="/resources/no-backdrop-w92.png" ng-if="item.backdrop_path == null" />

        <div>
            <a href="#!/show/{{item.id}}" ng-bind="item.name"></a>
            <span>({{item.first_air_date | date : 'yyyy'}})</span>
        </div>
    </div>

    <div ng-if="ssCtrl.similarShows.length == 0">No similar shows available</div>
`;

const similarShowsTemplateMd = `
    <div class="similar-movies--md">
        <h5 class="mb-4">Similar Shows</h5>

        <div class="d-flex align-items-stretch mb-3 pointered" ng-repeat="item in ssCtrl.similarShows" ng-click="ssCtrl.navigateTo(item.id)">
            <img class="mr-2" src="http://image.tmdb.org/t/p/w185{{item.backdrop_path}}" ng-if="item.backdrop_path" />
            <img class="mr-2" src="/resources/no-backdrop-w185.png" ng-if="!item.backdrop_path" />

            <div>
                <div class="similar-movies--md__title" ng-bind="item.name"></div>
                <div class="grey-text">{{item.first_air_date | date : 'yyyy'}}</div>
            </div>
        </div>

        <div ng-if="ssCtrl.similarShows.length == 0">No similar shows available</div>
    </div>
`;

const similarShowsTemplateMasonry = `
    <div masonry masonry-options="ssCtrl.masonryOptions">
        <div class="grid-sizer"></div>
        <div class="gutter-sizer"></div>
        <div class="masonry-brick" ng-repeat="item in ssCtrl.similarShows track by $index">
            <div class="card card--masonry" ng-click="ssCtrl.navigateTo(item.id)">
                <rating rating="item.vote_average" size="sm"></rating>
                
                <img class="card-img-top" src="http://image.tmdb.org/t/p/w185{{item.poster_path}}" alt="{{item.name}}" ng-show="item.poster_path">
                <img class="card-img-top" src="/resources/no-poster-w185.png" alt="{{item.name}}" ng-show="!item.poster_path">
                
                <div class="card-body">
                    <p class="card-title m-0" ng-bind="item.name"></p>

                    <p class="card-text text-right mt-2" ng-show="item.release_date || item.first_air_date">
                        <small ng-bind="(item.release_date || item.first_air_date) | date : 'yyyy'"></small>
                    </p>
                </div>
            </div>
        </div>
    </div>
`;

const similarShowsTemplateFlat = `
    <div class="row m-0">
        <div class="col-6 col-md-3 d-flex justify-content-center p-0 mb-4 position-relative" ng-repeat="item in ssCtrl.similarShows track by $index">
            <div class="flip-card flip-card--w154 flat-scale"> 
                <div class="flip-card__inner flat-box-shadow" ng-click="ssCtrl.flip($event);"> 
                    <div class="flip-card__face flip-card__face--front">
                        <img src="http://image.tmdb.org/t/p/w154{{item.poster_path}}" ng-if="item.poster_path" />
                        <img src="/resources/no-poster-w154.png" ng-if="!item.poster_path" />

                        <release-date date="item.first_air_date | date: 'yyyy'" color-theme="{{ssCtrl.colorTheme}}"></release-date>

                        <div class="flip-card__title flat-bg-{{ssCtrl.colorTheme}}" ng-bind="item.name"></div>
                    </div>

                    <div class="flip-card__face flip-card__face--back flat-bg-{{ssCtrl.colorTheme}}">
                        <div class="overflow-gradient overflow-gradient--{{ssCtrl.colorTheme}} h-75">
                            <div ng-bind="item.overview"></div>
                        </div>

                        <div class="flip-card__title flat-bg-{{ssCtrl.colorTheme}}" ng-click="ssCtrl.navigateTo(item.id); $event.stopPropagation();">Open</div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
`;

const similarShowsCtrl = function($location) {
  const ssCtrl = this;

  ssCtrl.$onInit = () => {
    ssCtrl.masonryOptions = masonryOptions;
  };

  ssCtrl.navigateTo = id => {
    $location.path(`/show/${id}`);
  };

  ssCtrl.flip = e => {
    $(e.currentTarget).toggleClass("flipped");
  };
};

angular.module("myApp").component("similarShows", {
  controller: similarShowsCtrl,
  controllerAs: "ssCtrl",
  template: getComponentTemplate({
    imdb: similarShowsTemplateImdb,
    md: similarShowsTemplateMd,
    masonry: similarShowsTemplateMasonry,
    flat: similarShowsTemplateFlat
  }),
  bindings: {
    similarShows: "=",
    colorTheme: "@?"
  }
});
