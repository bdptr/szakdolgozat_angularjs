"use strict";

const itemListTemplateImdb = `
    <table class="table" ng-if="ilCtrl.isFinished == true">
        <thead ng-if="!ilCtrl.hideTableHeader">
            <tr>
                <th></th>
                <th scope="col">Title & Description</th>
                <th ng-if="ilCtrl.showRating">Rating</th>
                <th scope="col" ng-if="ilCtrl.showVotes">Votes</th>
            </tr>
        </thead>

        <tbody>
            <tr ng-repeat="item in ilCtrl.list track by $index">
                <td style="width: 45px;">
                    <img src="http://image.tmdb.org/t/p/w45{{item.poster_path}}" ng-if="item.poster_path != null" />
                    <img src="/resources/no-poster-w45.png" ng-if="item.poster_path == null" />
                </td>

                <td class="align-middle">
                    <div>
                        <span ng-if="ilCtrl.showIndex" ng-bind="($index + 1) + '. '"></span>
                        <a href="#!/{{ilCtrl.type}}/{{item.id}}" ng-bind="item.title || item.name"></a>
                        <span ng-if="ilCtrl.showReleaseDate && (item.release_date || item.first_air_date)">({{(item.release_date || item.first_air_date) | date: (ilCtrl.dateFormat || '')}})</span>
                    </div>

                    <small ng-bind="item.overview" ng-if="ilCtrl.showOverview && item.overview"></small>
                </td>

                <td class="align-middle" ng-if="ilCtrl.showRating">
                    <div class="d-flex align-items-center" ng-if="item.vote_average">
                        <span class="fa fa-star mr-2"></span>
                        <b ng-bind="item.vote_average"></b>
                    </div>
                </td>

                <td class="align-middle" ng-if="ilCtrl.showVotes">
                    <div ng-bind="item.vote_count | number" ng-if="item.vote_count"></div>
                </td>
            </tr>
        </tbody>
    </table>
`;

const itemListTemplateMd = `
    <div ng-if="!ilCtrl.tableStyle">
        <div class="col s12 m6 l4 xl3" ng-repeat="item in ilCtrl.list track by $index" ng-if="ilCtrl.isFinished == true">
            <div class="card hoverable">
                <div class="card-image card-image--md--300" ng-class="{'waves-effect waves-block waves-light': ilCtrl.showOverview && item.overview}">
                    <img src="http://image.tmdb.org/t/p/w300{{item.backdrop_path}}" alt="{{item.title || item.name}}" ng-if="item.backdrop_path"" ng-class="{'pointered activator': ilCtrl.showOverview && item.overview}" />
                    <img src="/resources/no-backdrop-w300.png" alt="{{item.title || item.name}}" ng-if="!item.backdrop_path" ng-class="{'pointered activator': ilCtrl.showOverview && item.overview}" />
                </div>

                <div class="card-content">
                    <div class="d-flex align-items-center">
                        <h6 ng-bind="(item.release_date || item.first_air_date) | date: (ilCtrl.dateFormat || '')" ng-if="ilCtrl.showReleaseDate && (item.release_date || item.first_air_date)"></h6>
                        <div class="ml-auto" ng-if="ilCtrl.showRating && item.vote_average">
                            <rating rating="item.vote_average"></rating>
                        </div>
                    </div>

                    <h6 class="font-weight-bold truncate pointered w-100" ng-click="ilCtrl.navigateTo(item.id)">
                        <span ng-if="ilCtrl.showIndex" ng-bind="($index + 1) + '. '"></span>
                        <span ng-bind="item.title || item.name"></span>
                    </h6>
                </div>

                <div class="card-reveal p-3" ng-if="ilCtrl.showOverview && item.overview">
                    <span class="card-title">
                        <i class="material-icons right">close</i>
                    </span>

                    <p ng-bind="item.overview"></p>
                </div>
            </div>
        </div>
    </div>

    <div ng-if="ilCtrl.tableStyle">
        <ul class="collapsible popout" data-collapsible="accordion" watch>
            <li ng-repeat="item in ilCtrl.list track by $index" ng-if="ilCtrl.isFinished == true">
                <div class="collapsible-header valign-wrapper">
                    <img class="mr-4" src="http://image.tmdb.org/t/p/w92{{item.poster_path}}" alt="{{item.title || item.name}}" ng-show="item.poster_path" />
                    <img class="mr-4" src="/resources/no-poster-w92.png" alt="{{item.title || item.name}}" ng-show="!item.poster_path" />

                    <div class="flow-text w-100">
                        <div ng-bind="item.title || item.name"></div>
                        <div class="font-size-1" ng-show="ilCtrl.showReleaseDate && (item.release_date || item.first_air_date)" ng-bind="(item.release_date || item.first_air_date) | date"></div>
                    </div>

                    <a class="btn btn-floating waves-effect waves-light ml-4" ng-click="ilCtrl.navigateTo(item.id); $event.stopPropagation();">
                        <i class="material-icons m-0">arrow_forward</i>
                    </a>
                </div>

                <div class="collapsible-body">
                    <p class="flow-text" ng-bind="item.overview" ng-show="ilCtrl.showOverview && item.overview"></p>
                </div>
            </li>
        </ul>
    </div>
`;

const itemListTemplateMasonry = `
    <div masonry masonry-options="ilCtrl.masonryOptions">
        <div class="grid-sizer"></div>
        <div class="gutter-sizer"></div>
        <div class="masonry-brick" ng-repeat="item in ilCtrl.list track by $index">
            <div class="card card--masonry" ng-click="ilCtrl.navigateTo(item.id)">
                <div class="card--masonry__hover" ng-bind="(item.release_date || item.first_air_date) | date" ng-show="ilCtrl.showFancyReleaseDate && (item.release_date || item.first_air_date)"></div>

                <rating rating="item.vote_average" ng-show="ilCtrl.showRating && item.vote_average"></rating>
                
                <img class="card-img-top" src="http://image.tmdb.org/t/p/w300{{item.poster_path}}" alt="{{item.title || item.name}}" ng-show="item.poster_path">
                <img class="card-img-top" src="/resources/no-poster-w300.png" ng-show="!item.poster_path" />
                
                <div class="card-body">
                    <h5 class="card-title font-weight-bold">
                        <span ng-show="ilCtrl.showIndex" ng-bind="($index + 1) + '. '"></span>
                        <span ng-bind="item.title || item.name"></span>
                    </h5>
                
                    <p class="card-text" ng-bind="item.overview" ng-show="ilCtrl.showOverview && item.overview"></p>
                    <p class="card-text text-right" ng-show="ilCtrl.showReleaseDate && (item.release_date || item.first_air_date)">
                        <small ng-bind="(item.release_date || item.first_air_date) | date : 'yyyy'"></small>
                    </p>
                </div>
            </div>
        </div>
    </div>
`;

const itemListlTemplateFlat = `
    <div class="row m-0">
        <div class="col-6 col-md-3 col-xl-2 d-flex justify-content-center p-0 mb-4 position-relative" ng-repeat="item in ilCtrl.list track by $index">
            <div class="flip-card flat-scale"> 
                <div class="flip-card__inner flat-box-shadow" ng-click="ilCtrl.flip($event);"> 
                    <div class="flip-card__face flip-card__face--front">
                        <release-date date="(item.release_date || item.first_air_date) | date: (ilCtrl.dateFormat || '')" ng-if="ilCtrl.showReleaseDate" color-theme="{{ilCtrl.colorTheme}}"></release-date>

                        <rating rating="item.vote_average" ng-if="ilCtrl.showRating" color-theme="{{ilCtrl.colorTheme}}"></rating>

                        <img src="http://image.tmdb.org/t/p/w185{{item.poster_path}}" ng-if="item.poster_path" />
                        <img src="/resources/no-poster-w185.png" ng-if="!item.poster_path" />

                        <div class="flip-card__title flat-bg-{{ilCtrl.colorTheme}}">
                            <span ng-show="ilCtrl.showIndex" ng-bind="($index + 1) + '. '"></span>
                            <span ng-bind="item.title || item.name"></span>
                        </div>
                    </div>

                    <div class="flip-card__face flip-card__face--back flat-bg-{{ilCtrl.colorTheme}}">
                        <div class="overflow-gradient overflow-gradient--{{ilCtrl.colorTheme}} h-75">
                            <div ng-bind="item.overview"></div>
                        </div>

                        <div class="flip-card__title flat-bg-{{ilCtrl.colorTheme}}" ng-click="ilCtrl.navigateTo(item.id); $event.stopPropagation();">Open</div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
`;

const itemListCtrl = function($location) {
  const ilCtrl = this;

  ilCtrl.$onInit = () => {
    ilCtrl.isFinished = false;
    ilCtrl.masonryOptions = masonryOptions;
  };

  ilCtrl.$onChanges = changes => {
    if (changes.list.currentValue.length > 0) {
      ilCtrl.isFinished = true;
    }
  };

  ilCtrl.flip = e => {
    $(e.currentTarget).toggleClass("flipped");
  };

  ilCtrl.navigateTo = id => {
    $location.path(`/${ilCtrl.type}/${id}`);
  };
};

angular.module("myApp").component("itemList", {
  controller: itemListCtrl,
  controllerAs: "ilCtrl",
  template: getComponentTemplate({
    imdb: itemListTemplateImdb,
    masonry: itemListTemplateMasonry,
    md: itemListTemplateMd,
    flat: itemListlTemplateFlat
  }),
  bindings: {
    list: "<",
    type: "@?",

    tableStyle: "@?",
    hideTableHeader: "@?",

    showReleaseDate: "@?",
    showFancyReleaseDate: "@?",
    showIndex: "@?",
    showRating: "@?",
    showVotes: "@?",
    showOverview: "@?",

    dateFormat: "@?",
    colorTheme: "@?"
  }
});
