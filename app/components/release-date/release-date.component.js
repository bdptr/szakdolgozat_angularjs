"use strict";

const releaseDateTemplateFlat = `
  <div class="release-date--flat flat-bg-{{rdCtrl.colorTheme}}" ng-bind="rdCtrl.date" ng-if="rdCtrl.date"></div>
`;

const releaseDateCtrl = function() {
  const rdCtrl = this;
};

angular.module("myApp").component("releaseDate", {
  controller: releaseDateCtrl,
  controllerAs: "rdCtrl",
  template: getComponentTemplate({
    flat: releaseDateTemplateFlat
  }),
  bindings: {
    date: "<",
    colorTheme: "@?"
  }
});
