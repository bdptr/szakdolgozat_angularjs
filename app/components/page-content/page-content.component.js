"use strict";

const pageContentTemplateImdb = `
    <div class="page-content-wrapper page-content-wrapper--imdb">
        <div class="page-content--imdb">
            <page-header></page-header>

            <div class="p-4" ng-view></div>
        </div>
    </div>
`;

const pageContentTemplateMd = `
    <div class="page-content-wrapper page-content-wrapper--md">
        <ul id="side-nav-md" class="side-nav">
            <li class="side-nav-logo--md">
                <a href="#!/home">MDB</a>
            </li>

            <li><a class="waves-effect" href="#!/home">Home</a></li>

            <li><div class="divider"></div></li>

            <li><a class="subheader">Movies</a></li>
            <li><a class="waves-effect" href="#!/top-rated-movies">Top Rated</a></li>
            <li><a class="waves-effect" href="#!/discover-movies">Discover</a></li>

            <li><div class="divider"></div></li>

            <li><a class="subheader">TV Shows</a></li>
            <li><a class="waves-effect" href="#!/top-rated-shows">Top Rated</a></li>
            <li><a class="waves-effect" href="#!/discover-shows">Discover</a></li>
        </ul>

        <div class="page-content--md">
            <page-header></page-header>

            <div ng-view></div>
        </div>
    </div>
`;

const pageContentTemplateMasonry = `
    <div class="page-content-wrapper page-content-wrapper--masonry">
        <div class="page-content--masonry" style="padding-top: 65px;">
            <page-header></page-header>

            <div class="p-4" ng-view></div>
        </div>
    </div>
`;

const pageContentTemplateFlat = `
    <div class="page-content-wrapper page-content-wrapper--flat">
        <div class="page-content--flat" style="padding-top: 40px;display: flex;flex-direction: column;flex: 1 1 100%;">
            <page-header></page-header>

            <div class="p-4" style="display: flex;flex-direction: column;flex: 1 1 100%;" ng-view></div>
        </div>
    </div>
`;

const pageContentCtrl = function() {
  const pcCtrl = this;
};

angular.module("myApp").component("pageContent", {
  controller: pageContentCtrl,
  controllerAs: "pcCtrl",
  template: getComponentTemplate({
    imdb: pageContentTemplateImdb,
    md: pageContentTemplateMd,
    masonry: pageContentTemplateMasonry,
    flat: pageContentTemplateFlat
  })
});
