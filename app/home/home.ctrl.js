"use strict";

function homeCtrl(ApiService, $location, $anchorScroll) {
  const ctrl = this;
  ctrl.nowPlaying = [];
  ctrl.upcoming = [];
  ctrl.onTheAir = [];
  ctrl.airingToday = [];
  ctrl.selectedCategory = "nowPlaying";
  ctrl.masonryOptions = masonryOptions;

  ApiService.getNowPlayingMovies().then(response => {
    ctrl.nowPlaying = response.results;
  });

  ApiService.getUpcomingMovies().then(response => {
    ctrl.upcoming = response.results;
  });

  ApiService.getOnTheAirShows().then(response => {
    ctrl.onTheAir = response.results;
  });

  ApiService.getAiringTodayShows().then(response => {
    ctrl.airingToday = response.results;
  });

  ctrl.selectCategory = category => {
    switch (category) {
      case "nowPlaying":
        ctrl.selectedCategory = "nowPlaying";
        break;
      case "upcoming":
        ctrl.selectedCategory = "upcoming";
        break;
      case "onTheAir":
        ctrl.selectedCategory = "onTheAir";
        break;
      case "airingToday":
        ctrl.selectedCategory = "airingToday";
        break;
    }
  };

  ctrl.navigateToMovie = id => {
    $location.path(`/movie/${id}`);
  };

  ctrl.goTo = id => {
    $location.hash(id);
    $anchorScroll();
  };
}

angular
  .module("myApp.home", ["ngRoute"])
  .controller("HomeCtrl", [
    "ApiService",
    "$location",
    "$anchorScroll",
    homeCtrl
  ]);
