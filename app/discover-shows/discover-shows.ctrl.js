"use strict";

function discoverShowsCtrl(ApiService, $timeout) {
  const ctrl = this;
  ctrl.currentPage = 1;
  ctrl.showsToDiscover = [];
  ctrl.busy = false;

  ctrl.loadMore = () => {
    if (ctrl.busy) return;
    ctrl.busy = true;

    ApiService.getShowsToDiscover(ctrl.currentPage).then(response => {
      ++ctrl.currentPage;
      ctrl.showsToDiscover = ctrl.showsToDiscover.concat(response.results);
      $timeout(() => {
        ctrl.busy = false;
      }, 1000);
    });
  };

  ctrl.loadMore();
}

angular
  .module("myApp.discoverShows", ["ngRoute"])
  .controller("DiscoverShowsCtrl", [
    "ApiService",
    "$timeout",
    discoverShowsCtrl
  ]);
