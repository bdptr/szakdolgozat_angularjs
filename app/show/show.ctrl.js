'use strict';

function showCtrl(ApiService, $routeParams, YOUTUBE_EMBED_PREFIX) {
  const ctrl = this;
  ctrl.show;
  ctrl.trailer;
  ctrl.createdBy = '';
  ctrl.genres = '';

  ApiService.getShowById($routeParams.id).then((response) => {
    ctrl.show = response;
    console.log(ctrl.show)

    if (ctrl.show.videos.results.length > 0) {
      ctrl.trailer = YOUTUBE_EMBED_PREFIX + ctrl.show.videos.results[0].key;
    }

    ctrl.createdBy = joinItems(ctrl.show.created_by);
    ctrl.genres = joinItems(ctrl.show.genres);
  });

  const joinItems = (array) => {
    let newArray = [];
    array.map((item) => newArray.push(item.name));
    return newArray.join(', ');
  }
}

angular.module('myApp.show', ['ngRoute'])
  .controller('ShowCtrl', ['ApiService', '$routeParams', 'YOUTUBE_EMBED_PREFIX', showCtrl]);