'use strict';

const apiService = function ($http, API_KEY, $q) {
    const apiServiceBase = (url) => {
        const q = $q.defer();
        $http
            .get(url)
            .success((response) => {
                q.resolve(response);
            })
            .error((err) => {
                q.reject(err);
            });
        return q.promise;
    }

    this.getNowPlayingMovies = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/movie/now_playing?${API_KEY}&region=US`);
    }

    this.getUpcomingMovies = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/movie/upcoming?${API_KEY}&region=US`);
    }

    this.getMovieById = (id) => {
        return apiServiceBase(`https://api.themoviedb.org/3/movie/${id}?${API_KEY}&region=US&append_to_response=videos,similar,credits`);
    }

    this.getShowById = (id) => {
        return apiServiceBase(`https://api.themoviedb.org/3/tv/${id}?${API_KEY}&region=US&append_to_response=videos,similar,credits`);
    }

    this.getTopRatedMovies = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/movie/top_rated?${API_KEY}&region=US`);
    }

    this.getTopRatedShows = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/tv/top_rated?${API_KEY}&region=US`);
    }

    this.getMoviesToDiscover = (page) => {
        return apiServiceBase(`https://api.themoviedb.org/3/discover/movie?${API_KEY}&page=${page}&sort_by=popularity.desc&region=US`);
    }

    this.getShowsToDiscover = (page) => {
        return apiServiceBase(`https://api.themoviedb.org/3/discover/tv?${API_KEY}&page=${page}&sort_by=popularity.desc&region=US`);
    }

    this.getOnTheAirShows = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/tv/on_the_air?${API_KEY}&region=US`);
    }

    this.getAiringTodayShows = () => {
        return apiServiceBase(`https://api.themoviedb.org/3/tv/airing_today?${API_KEY}&region=US`);
    }

    this.searchMovies = (searchValue) => {
        return apiServiceBase(`https://api.themoviedb.org/3/search/multi?${API_KEY}&query="${searchValue}&region=US"`);
    }
}

angular.module('myApp')
    .service('ApiService', ['$http', 'API_KEY', '$q', apiService]);