'use strict';

const searchService = function ($rootScope, $location) {
    this.searchValue = '';

    $rootScope.$on('$locationChangeSuccess', () => {
        const locationPathArray = $location.$$path.split('/');
        
        if (locationPathArray[1] === 'search') {
            this.setSearchValue(locationPathArray[2]);
        }
        else {
            this.setSearchValue('');
        }
    });

    this.getSearchValue = () => {
        return this.searchValue;
    }

    this.setSearchValue = (searchValue) => {
        let joinedSearchValue = searchValue.split('+').join(' ');
        this.searchValue = joinedSearchValue;
        $rootScope.$broadcast('SearchService.setSearchValue');
    }
}

angular.module('myApp')
    .service('SearchService', ['$rootScope', '$location', searchService]);