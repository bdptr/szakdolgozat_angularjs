"use strict";

angular
  .module("myApp", [
    "ngRoute",
    "wu.masonry",
    "angular-loading-bar",
    "infinite-scroll",
    "ui.materialize",
    "angular.css.injector",
    "myApp.main",
    "myApp.home",
    "myApp.topRatedMovies",
    "myApp.topRatedShows",
    "myApp.discoverMovies",
    "myApp.discoverShows",
    "myApp.movie",
    "myApp.show",
    "myApp.search"
  ])
  .config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
      "self",
      "https://www.youtube.com/embed/**"
    ]);
  })
  .config(function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.spinnerTemplate = `
      <div id="loading-bar-spinner">
        <div class="spinner">
          <div class="bounce1"></div>
          <div class="bounce2"></div>
          <div class="bounce3"></div>
        </div>
      </div>`;
  });
