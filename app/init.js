"use strict";

var head = document.getElementsByTagName("head")[0];

function injectCss(...filenames) {
  filenames.map(fn => {
    let style = document.createElement("link");
    style.href = fn;
    style.type = "text/css";
    style.rel = "stylesheet";
    head.appendChild(style);
  });
}

// --- imdb, md, masonry, flat
const appStyle = "flat";

switch (appStyle) {
  // --- imdb
  case "imdb":
    injectCss("third-party/bootstrap-4/css/bootstrap.css", "third-party/font-awesome-4.7.0/css/font-awesome.min.css", "theme-imdb.css");
    break;

  // --- md
  case "md":
    injectCss(
      "https://fonts.googleapis.com/icon?family=Material+Icons|Roboto",
      "third-party/font-awesome-4.7.0/css/font-awesome.min.css",
      "bower_components/materialize/dist/css/materialize.min.css",
      "third-party/bootstrap-4/css/bootstrap-utilities.css",
      "theme-md.css"
    );
    break;

  // --- masonry
  case "masonry":
    injectCss(
      "https://fonts.googleapis.com/css?family=Lobster|Open+Sans",
      "third-party/font-awesome-4.7.0/css/font-awesome.min.css",
      "third-party/bootstrap-4/css/bootstrap.css",
      "theme-masonry.css"
    );
    break;

  // --- flat
  case "flat":
    injectCss(
      "third-party/bootstrap-4/css/bootstrap.css",
      "third-party/font-awesome-4.7.0/css/font-awesome.min.css",
      "bower_components/material-design-color-palette/css/material-design-color-palette.min.css",
      "theme-flat.css"
    );
    break;
}

const getComponentTemplate = templateObject => {
  return templateObject[appStyle];
};

const masonryOptions = {
  itemSelector: ".masonry-brick",
  columnWidth: ".grid-sizer",
  gutter: ".gutter-sizer",
  percentPosition: true,
  transitionDuration: "0s"
};
