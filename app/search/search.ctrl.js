"use strict";

function searchCtrl(ApiService, $routeParams) {
  const ctrl = this;
  ctrl.results = false;
  ctrl.movies = [];
  ctrl.shows = [];
  ctrl.resultsText = "";

  ApiService.searchMovies($routeParams.searchValue).then(response => {
    ctrl.resultsText = $routeParams.searchValue.split("+").join(" ");
    if (response.results.length > 0) {
      ctrl.results = true;
      ctrl.movies = response.results
        .filter(result => result.media_type == "movie")
        .sort((a, b) => comparePopularity(a, b));
      ctrl.shows = response.results
        .filter(result => result.media_type == "tv")
        .sort((a, b) => comparePopularity(a, b));
    }
  });

  const comparePopularity = (a, b) => {
    return b.popularity - a.popularity;
  };
}

angular
  .module("myApp.search", ["ngRoute"])
  .controller("SearchCtrl", ["ApiService", "$routeParams", searchCtrl]);
