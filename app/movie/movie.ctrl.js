"use strict";

function movieCtrl($routeParams, ApiService, YOUTUBE_EMBED_PREFIX) {
  const ctrl = this;
  ctrl.movie;
  ctrl.trailer;
  ctrl.directors = "";
  ctrl.writers = "";
  ctrl.novelists = "";
  ctrl.genres = "";
  ctrl.masonryOptions = masonryOptions;

  ApiService.getMovieById($routeParams.id).then(response => {
    ctrl.movie = response;

    if (ctrl.movie.videos.results.length > 0) {
      ctrl.trailer = YOUTUBE_EMBED_PREFIX + ctrl.movie.videos.results[0].key;
    }

    ctrl.directors = joinItems(
      ctrl.movie.credits.crew.filter(crew => crew.job === "Director")
    );
    ctrl.writers = joinItems(
      ctrl.movie.credits.crew.filter(crew => crew.job === "Writer")
    );
    ctrl.novelists = joinItems(
      ctrl.movie.credits.crew.filter(crew => crew.job === "Novel")
    );
    ctrl.genres = joinItems(ctrl.movie.genres);
  });

  const joinItems = array => {
    let newArray = [];
    array.map(item => newArray.push(item.name));
    return newArray.join(", ");
  };
}

angular
  .module("myApp.movie", ["ngRoute"])
  .controller("MovieCtrl", [
    "$routeParams",
    "ApiService",
    "YOUTUBE_EMBED_PREFIX",
    movieCtrl
  ]);
