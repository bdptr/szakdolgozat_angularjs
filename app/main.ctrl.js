"use strict";

function mainCtrl() {
  const mainCtrl = this;
}

angular.module("myApp.main", ["ngRoute"]).controller("MainCtrl", mainCtrl);
