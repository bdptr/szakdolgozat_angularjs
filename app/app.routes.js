const getTemplateUrl = (page) => {
    return `${page}/${page}-${appStyle}.html`;
}

angular.module('myApp').
    config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
            when('/top-rated-movies', {
                templateUrl: getTemplateUrl('top-rated-movies'),
                controller: 'TopRatedMoviesCtrl',
                controllerAs: 'ctrl'
            }).
            when('/top-rated-shows', {
                templateUrl: getTemplateUrl('top-rated-shows'),
                controller: 'TopRatedShowsCtrl',
                controllerAs: 'ctrl'
            }).
            when('/discover-movies', {
                templateUrl: getTemplateUrl('discover-movies'),
                controller: 'DiscoverMoviesCtrl',
                controllerAs: 'ctrl'
            }).
            when('/discover-shows', {
                templateUrl: getTemplateUrl('discover-shows'),
                controller: 'DiscoverShowsCtrl',
                controllerAs: 'ctrl'
            }).
            when('/movie/:id', {
                templateUrl: getTemplateUrl('movie'),
                controller: 'MovieCtrl',
                controllerAs: 'ctrl'
            }).
            when('/show/:id', {
                templateUrl: getTemplateUrl('show'),
                controller: 'ShowCtrl',
                controllerAs: 'ctrl'
            }).
            when('/home', {
                templateUrl: getTemplateUrl('home'),
                controller: 'HomeCtrl',
                controllerAs: 'ctrl',
                reloadOnSearch: false
            }).
            when('/search/:searchValue', {
                templateUrl: getTemplateUrl('search'),
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            }).
            otherwise({ redirectTo: '/home' });
    }]);