"use strict";

function topRatedShowsCtrl(ApiService) {
  const ctrl = this;
  ctrl.topRatedShows = [];

  ApiService.getTopRatedShows().then(response => {
    ctrl.topRatedShows = response.results;
  });
}

angular
  .module("myApp.topRatedShows", ["ngRoute"])
  .controller("TopRatedShowsCtrl", ["ApiService", topRatedShowsCtrl]);
